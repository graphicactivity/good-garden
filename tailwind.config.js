module.exports = {
  theme: {
    fontFamily: {
      display: ['Alice', 'serif'],
      body: ['Open Sans', 'sans-serif'],
    },
    colors: {
        white: '#ffffff',
        greenlighter: '#c7d0b3',
        green: '#bac7a7',
        greendarker: '#889e81',
        greendarkest: '#698474',
        black: '#111111',
        red: '#c10',
    },
    extend: {},
  },
  variants: {},
  plugins: [],
}
